<?php
/**
 * 关键词商品页API
 *
 * @link: https://www.haodanku.com/api/detail/show/5
 *
 * User: Ken.Zhang <kenphp@yeah.net>
 * Date: 2019/9/22
 * Time: 21:01
 */
namespace OpenSDK\HaoDanKu\Requests;

use OpenSDK\HaoDanKu\Interfaces\Request;

class ItemKeywordSearchRequest implements Request
{

    /**
     * 接口
     *
     * @var string
     */
    public $method = '/get_keyword_items';

    /**
     * 请求方式
     *
     * @var string
     */
    public $requestType = 'get';

    private $keyword;   // 搜索关键词 支持宝贝ID搜索即keyword=itemid（由于存在特殊符号搜索的关键词必须进行两次urlencode编码）

    private $shopid;    // 根据店铺id搜索商品
                        // （需要注意的是店铺id搜索暂不支持筛选和排序，如果链接里有关键词和shopid优先搜索店铺id商品）

    private $back = 500;// 每页返回条数（请在1,2,10,20,50,100,120,200,500,1000中选择一个数值返回）

    private $cid;       // 0全部，1女装，2男装，3内衣，4美妆，5配饰，6鞋品，7箱包，8儿童，9母婴，10居家，11美食，12数码，
                        // 13家电，14其他，15车品，16文体

    private $min_id = 1;// 分页，用于实现类似分页抓取效果，来源于上次获取后的数据的min_id值，默认开始请求值为1
                        //（该方案比单纯123分页的优势在于：数据更新的情况下保证不会重复也无需关注和计算页数）

    private $price_min; // 	券后价筛选，筛选大于等于所设置的券后价的商品

    private $price_max; // 券后价筛选，筛选小于等于所设置的券后价的商品

    private $sale_min;  // 销量筛选，筛选大于等于所设置的销量的商品

    private $sale_max;  // 销量筛选，筛选小于等于所设置的销量的商品

    private $coupon_min;// 券金额筛选，筛选大于等于所设置的券金额的商品

    private $coupon_max;// 券金额筛选，筛选小于等于所设置的券金额的商品

    private $type;      // 商品筛选类型：type=1是今日上新，type=2是9.9包邮，type=3是30元封顶，type=4是聚划算，
                        // type=5是淘抢购，type=6是0点过夜单，type=7是预告单，type=8是品牌单，type=9是天猫商品，
                        // type=10是视频单

    private $apiParams = [];



    public function setKeyword($val)
    {
        $this->keyword = urldecode($val);
        $this->apiParams['keyword'] = urldecode($val);
    }

    public function setShopid($val)
    {
        $this->shopid = (int)$val;
        $this->apiParams['shopid'] = (int)$val;
    }

    public function setBack($val)
    {
        $this->back = (int)$val;
        $this->apiParams['back'] = (int)$val;
    }

    public function setCid($val)
    {
        $this->cid = (int)$val;
        $this->apiParams['cid'] = (int)$val;
    }

    public function setMinId($val)
    {
        $this->min_id = (int)$val;
        $this->apiParams['min_id'] = (int)$val;
    }

    public function setPriceMin($val)
    {
        $this->price_min = (int)$val;
        $this->apiParams['price_min'] = (int)$val;
    }

    public function setPriceMax($val)
    {
        $this->price_max = (int)$val;
        $this->apiParams['price_max'] = (int)$val;
    }

    public function setSaleMin($val)
    {
        $this->sale_min = (int)$val;
        $this->apiParams['sale_min'] = (int)$val;
    }

    public function setSaleMax($val)
    {
        $this->sale_max = (int)$val;
        $this->apiParams['sale_max'] = (int)$val;
    }

    public function setCouponMin($val)
    {
        $this->coupon_min = (int)$val;
        $this->apiParams['coupon_min'] = (int)$val;
    }

    public function setCouponMax($val)
    {
        $this->coupon_max = (int)$val;
        $this->apiParams['coupon_max'] = (int)$val;
    }

    public function setType($val)
    {
        $this->type = (int)$val;
        $this->apiParams['type'] = (int)$val;
    }

    /**
     * 获取参数
     */
    public function getParams()
    {
        return $this->apiParams;
    }

}