<?php
/**
 * 失效举报API
 *
 * @link: https://www.haodanku.com/api/detail/show/10
 *
 * User: Ken.Zhang <kenphp@yeah.net>
 * Date: 2019/9/22
 * Time: 21:01
 */
namespace OpenSDK\HaoDanKu\Requests;

use OpenSDK\HaoDanKu\Interfaces\Request;

class InvalidItemReportRequest implements Request
{

    /**
     * 接口
     *
     * @var string
     */
    public $method = '/api_report';

    /**
     * 请求方式
     *
     * @var string
     */
    public $requestType = 'get';

    private $itemid;    // 宝贝ID

    private $apiParams = [];



    public function setItemId($val)
    {
        $this->itemid = (string)$val;
        $this->apiParams['itemid'] = (string)$val;
    }

    /**
     * 获取参数
     */
    public function getParams()
    {
        return $this->apiParams;
    }

}