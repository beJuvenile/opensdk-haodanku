<?php
/**
 * 达人文章API
 *
 * User: Ken.Zhang <kenphp@yeah.net>
 * Date: 2019/9/22
 * Time: 21:01
 */
namespace OpenSDK\HaoDanKu\Requests;

use OpenSDK\HaoDanKu\Interfaces\Request;

class TalentInfoItemRequest implements Request
{

    /**
     * 接口
     *
     * @var string
     */
    public $method = '/talent_article';

    /**
     * 请求方式
     *
     * @var string
     */
    public $requestType = 'get';

    private $id;    // 达人文章ID

    private $apiParams = [];


    public function setId($val)
    {
        $this->id = (int)$val;
        $this->apiParams['id'] = (int)$val;
    }

    /**
     * 获取参数
     */
    public function getParams()
    {
        return $this->apiParams;
    }

}