<?php
/**
 * 精选专题API
 *
 * @link: https://www.haodanku.com/api/detail/show/11
 *
 * User: Ken.Zhang <kenphp@yeah.net>
 * Date: 2019/9/22
 * Time: 21:01
 */
namespace OpenSDK\HaoDanKu\Requests;

use OpenSDK\HaoDanKu\Interfaces\Request;

class SubjectRequest implements Request
{

    /**
     * 接口
     *
     * @var string
     */
    public $method = '/get_subject';

    /**
     * 请求方式
     *
     * @var string
     */
    public $requestType = 'get';

    private $apiParams = [];


    /**
     * 获取参数
     */
    public function getParams()
    {
        return $this->apiParams;
    }

}