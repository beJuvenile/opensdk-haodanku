<?php
/**
 * 达人说API
 *
 * @link: https://www.haodanku.com/api/detail/show/24
 *
 * User: Ken.Zhang <kenphp@yeah.net>
 * Date: 2019/9/22
 * Time: 21:01
 */
namespace OpenSDK\HaoDanKu\Requests;

use OpenSDK\HaoDanKu\Interfaces\Request;

class TalentInfoRequest implements Request
{

    /**
     * 接口
     *
     * @var string
     */
    public $method = '/talent_info';

    /**
     * 请求方式
     *
     * @var string
     */
    public $requestType = 'get';

    private $talentcat; // 大家都在逛类目，文章类别（0.全部，1.好物,2.潮流,3.美食,4.生活）

    private $apiParams = [];



    public function setTalentcat($val)
    {
        $this->talentcat = (int)$val;
        $this->apiParams['talentcat'] = (int)$val;
    }

    /**
     * 获取参数
     */
    public function getParams()
    {
        return $this->apiParams;
    }

}