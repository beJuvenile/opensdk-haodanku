<?php
/**
 * 超级分类
 *
 * @link: https://www.haodanku.com/api/detail/show/9
 *
 * User: Ken.Zhang <kenphp@yeah.net>
 * Date: 2019/9/22
 * Time: 21:01
 */
namespace OpenSDK\HaoDanKu\Requests;

use OpenSDK\HaoDanKu\Interfaces\Request;

class SuperCategoryRequest implements Request
{

    /**
     * 接口
     *
     * @var string
     */
    public $method = '/super_classify';

    /**
     * 请求方式
     *
     * @var string
     */
    public $requestType = 'get';

    private $apiParams = [];

    /**
     * 获取参数
     */
    public function getParams()
    {
        return $this->apiParams;
    }

}