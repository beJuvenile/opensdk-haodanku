<?php
/**
 * 高佣api调用
 *
 * @link: https://www.haodanku.com/api/detail/show/15
 *
 * User: Ken.Zhang <kenphp@yeah.net>
 * Date: 2019/9/22
 * Time: 21:01
 */
namespace OpenSDK\HaoDanKu\Requests;

use OpenSDK\HaoDanKu\Interfaces\Request;

class TbPrivilegeLinkRequest implements Request
{

    /**
     * 接口
     *
     * @var string
     */
    public $method = '/ratesurl';

    /**
     * 请求方式
     *
     * @var string
     */
    public $requestType = 'get';

    private $itemid;    // 宝贝ID

    private $pid;       // 推广位ID（*必要 需是授权淘宝号下的推广位，如果请求的时候携带了渠道id请求，则该pid需是渠道管理下面的渠道专属推广位）

    private $relation_id;// 渠道ID

    private $tb_name;   // 授权后的淘宝名称（*必要，多授权淘宝号时用于区分哪个淘宝账户的）

    private $activityid;// 阿里妈妈推广券ID （选填）

    private $me;        // 营销计划（选填）

    private $access_token;// 授权之后的淘宝授权token（选填）

    private $apiParams = [];


    public function setItemId($val)
    {
        $this->itemid = (int)$val;
        $this->apiParams['itemid'] = (int)$val;
    }

    public function setPid($val)
    {
        $this->pid = (string)$val;
        $this->apiParams['pid'] = (string)$val;
    }

    public function setRelationId($val)
    {
        $this->relation_id = (string)$val;
        $this->apiParams['relation_id'] = (string)$val;
    }

    public function setTbName($val)
    {
        $this->tb_name = (string)$val;
        $this->apiParams['tb_name'] = (string)$val;
    }

    public function setActivityId($val)
    {
        $this->activityid = (string)$val;
        $this->apiParams['activityid'] = (string)$val;
    }

    public function setMe($val)
    {
        $this->me = (string)$val;
        $this->apiParams['me'] = (string)$val;
    }

    public function setAccessToken($val)
    {
        $this->access_token = (string)$val;
        $this->apiParams['access_token'] = (string)$val;
    }

    /**
     * 获取参数
     */
    public function getParams()
    {
        return $this->apiParams;
    }

}