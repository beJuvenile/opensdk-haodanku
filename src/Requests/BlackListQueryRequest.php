<?php
/**
 * 单个黑名单掌柜名查询API
 *
 * @link: https://www.haodanku.com/api/detail/show/7
 *
 * User: Ken.Zhang <kenphp@yeah.net>
 * Date: 2019/9/22
 * Time: 21:01
 */
namespace OpenSDK\HaoDanKu\Requests;

use OpenSDK\HaoDanKu\Interfaces\Request;

class BlackListQueryRequest implements Request
{

    /**
     * 接口
     *
     * @var string
     */
    public $method = '/check_sellernick';

    /**
     * 请求方式
     *
     * @var string
     */
    public $requestType = 'get';

    private $sellernick;    // 掌柜名

    private $apiParams = [];


    public function setSellernick($val)
    {
        $this->sellernick = (string)$val;
        $this->apiParams['sellernick'] = (string)$val;
    }

    /**
     * 获取参数
     */
    public function getParams()
    {
        return $this->apiParams;
    }

}