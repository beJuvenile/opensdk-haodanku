<?php
/**
 * 商品更新API
 *
 * @link: https://www.haodanku.com/api/detail/show/2
 *
 * User: Ken.Zhang <kenphp@yeah.net>
 * Date: 2019/9/22
 * Time: 21:01
 */
namespace OpenSDK\HaoDanKu\Requests;

use OpenSDK\HaoDanKu\Interfaces\Request;

class ItemUpdateRequest implements Request
{

    /**
     * 接口
     *
     * @var string
     */
    public $method = '/update_item';

    /**
     * 请求方式
     *
     * @var string
     */
    public $requestType = 'get';

    private $sort;      // 更新排序（1好单指数，2月销量，3近两小时销量，4当天销量，5在线人数，6活动开始时间）

    private $back = 500;// 每页返回条数（请在1,2,10,20,50,100,120,200,500,1000中选择一个数值返回）

    private $min_id = 1;// 分页，用于实现类似分页抓取效果，来源于上次获取后的数据的min_id值，默认开始请求值为1
                        // （该方案比单纯123分页的优势在于：数据更新的情况下保证不会重复也无需关注和计算页数）

    private $apiParams = [];


    public function setSort($sort=0)
    {
        $this->sort = (int)$sort;
        $this->apiParams['sort'] = (int)$sort;
    }

    public function setBack($val)
    {
        $this->back = (int)$val;
        $this->apiParams['back'] = (int)$val;
    }

    public function setMinId($val)
    {
        $this->min_id = (int)$val;
        $this->apiParams['min_id'] = (int)$val;
    }

    /**
     * 获取参数
     */
    public function getParams()
    {
        return $this->apiParams;
    }

}