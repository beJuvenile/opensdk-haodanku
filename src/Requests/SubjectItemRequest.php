<?php
/**
 * 专题商品API
 *
 * User: Ken.Zhang <kenphp@yeah.net>
 * Date: 2019/9/22
 * Time: 21:01
 */
namespace OpenSDK\HaoDanKu\Requests;

use OpenSDK\HaoDanKu\Interfaces\Request;

class SubjectItemRequest implements Request
{

    /**
     * 接口
     *
     * @var string
     */
    public $method = '/get_subject_item';

    /**
     * 请求方式
     *
     * @var string
     */
    public $requestType = 'get';

    private $id;    // 专题ID

    private $apiParams = [];



    public function setId($val)
    {
        $this->id = (int)$val;
        $this->apiParams['id'] = (int)$val;
    }

    /**
     * 获取参数
     */
    public function getParams()
    {
        return $this->apiParams;
    }

}