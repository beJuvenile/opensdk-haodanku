<?php
/**
 * 完整黑名单库API
 *
 * @link: https://www.haodanku.com/api/detail/show/7
 *
 * User: Ken.Zhang <kenphp@yeah.net>
 * Date: 2019/9/22
 * Time: 21:01
 */
namespace OpenSDK\HaoDanKu\Requests;

use OpenSDK\HaoDanKu\Interfaces\Request;

class BlackListRequest implements Request
{

    /**
     * 接口
     *
     * @var string
     */
    public $method = '/blacklist';

    /**
     * 请求方式
     *
     * @var string
     */
    public $requestType = 'get';

    private $apiParams = [];

    /**
     * 获取参数
     */
    public function getParams()
    {
        return $this->apiParams;
    }

}