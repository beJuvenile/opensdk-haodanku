<?php
/**
 * 失效商品列表API
 *
 * @link: https://www.haodanku.com/api/detail/show/4
 *
 * User: Ken.Zhang <kenphp@yeah.net>
 * Date: 2019/9/22
 * Time: 21:01
 */
namespace OpenSDK\HaoDanKu\Requests;

use OpenSDK\HaoDanKu\Interfaces\Request;

class ItemDownRequest implements Request
{

    /**
     * 接口
     *
     * @var string
     */
    public $method = '/get_down_items';

    /**
     * 请求方式
     *
     * @var string
     */
    public $requestType = 'get';

    private $start; // 小时点数，如0点是0、13点是13（最小值是0，最大值是23）

    private $end;   // 小时点数，如0点是0、13点是13（最小值是0，最大值是23）

    private $apiParams = [];


    public function seStart($val)
    {
        $this->start = (int)$val;
        $this->apiParams['start'] = (int)$val;
    }

    public function setEnd($val)
    {
        $this->end = (int)$val;
        $this->apiParams['end'] = (int)$val;
    }

    /**
     * 获取参数
     */
    public function getParams()
    {
        return $this->apiParams;
    }

}