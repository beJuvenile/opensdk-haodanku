# opensdk-haodanku

#### 介绍
本类库是对好单库开放平台API的封装  
接口文档请参见 [好单库开放平台](https://www.haodanku.com/api/detail)

#### 使用示例
~~~php
require 'vendor/autoload.php';

use OpenSDK\HaoDanKu\Client;
use OpenSDK\HaoDanKu\Requests\HotWordsRequest;

$c = new Client();
$c->appKey = 'You are appKey';
$req = new HotWordsRequest();
$c->setRequest($req);
$result = $c->execute();

var_dump($result);
~~~