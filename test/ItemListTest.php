<?php
/**
 * Created by PhpStorm.
 * User: Ken.Zhang
 * Date: 2019/9/23
 * Time: 11:46
 */
require '../vendor/autoload.php';

use OpenSDK\HaoDanKu\Client;
use OpenSDK\HaoDanKu\Requests\ItemListRequest;

class ItemListTest
{

    private $appKey = 'zhuanplus';

    public function __invoke()
    {
        $c = new Client();
        $c->appKey = $this->appKey;
        $req = new ItemListRequest();
        $c->setRequest($req);
        $result = $c->execute();

        var_dump($result);
    }

}

(new ItemListTest())();



/*
$c = new Client();
$c->appKey = '5d84bcc8bf121';
$c->appSecret = 'dab025a5213aef8e94a8f2dc52d37e2a';
$req = new HotWordsRequest();
$c->setRequest($req);
$result = $c->execute();
echo 1;
var_dump($result);
*/